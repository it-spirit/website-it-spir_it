module.exports = function (eleventyConfig) {

  /**
   * Ignore the internal documents for production builds.
   *
   * This is currently the __docs folder.
   */
  if (process.env.ELEVENTY_ENV === 'prod') {
    eleventyConfig.ignores.add('./src/__docs/');
  }

  return {
    dir: {
      input: 'src',
      output: 'dist',
    },
  };
};
