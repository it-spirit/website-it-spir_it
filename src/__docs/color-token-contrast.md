---
title: Color Token Contrast
---

> The following are contrast safe combinations as calculated for _normal_ text based on WCAG AA 4.5

## color-primary
  - `color-on-primary`
  - `color-secondary`

## color-on-primary
  - `color-primary`

## color-secondary
  - `color-primary`
  - `color-on-secondary`

## color-on-secondary
  - `color-secondary`
